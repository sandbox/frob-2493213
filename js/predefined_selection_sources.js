(function ($) {
  Drupal.behaviors.predefined_sources = {
    attach: function (context, settings) {
      var $selector = $('.filefield-source-predefined .predefined-selector', context);

      $('.predefined-option-image').click(function(e) {
        var option = $(this).data('option');
        $selector.val(option);
      });
    }//end of attach
  };
}) (jQuery);
